package com.tp3.tp3;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Tp3ApplicationTests {

    private final static Tp3Application kilo = new Tp3Application();
    private final static Logger logger = LoggerFactory.getLogger(Tp3ApplicationTests.class);

    @Test
    public void verifierCharacter() throws Exception{
        String dd = kilo.kilometer("toto");
        assertEquals("0.0",dd);
        logger.info("test vérifier l'orsque l'utilisateur insert un charactère");
    }

    @Test
    public void verifierNegatif() throws Exception{
        String dd = kilo.kilometer(-10);
        assertEquals("0.0",dd);
        logger.info("test vérifier l'orsque l'utilisateur insert un nombre négative");
    }
    @Test
    public void verifierNormal() throws Exception{
        String dd = kilo.kilometer(99);
        assertEquals("44.81",dd);
        logger.info("test vérifier l'orsque l'utilisateur insert un nombre > 60");
    }

}
