package com.tp3.tp3;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp3Application {
    // testBranch master

    /* 82km
     *   10 * 1,5 =
     *   30 * 0.4 =
     *   20 * 0.55 =
     *22:1  * 6,81 =
     */

    private static final Logger logger = LoggerFactory.getLogger(Tp3Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Tp3Application.class, args);
        double[] kilometers = {-10,0,0.1,17.123,39.5,61,81,99};
        for (double kilo :
                kilometers) {
            System.out.println( kilometer(kilo) );
        }
        kilometer("test");
        logger.error("Test");
        //test
    }

    public static <T> String kilometer(T s) {
        try {

            double dist = Double.parseDouble(String.valueOf(s));
            double remb = 0.0;

            if (dist < 0){
                return "0.0";
            }
            else if (dist < 10.0){
                logger.info("the dist is < 10.0 and all good");
                remb = dist * 1.50;

            }else if (dist < 40 ){
                logger.info("the dist is < 40.0 and all good");
                remb = 10 * 1.50 + (dist-10) * 0.40;

            }else if (dist <= 60 ){
                logger.info("the dist is < 60.0 and all good");
                remb =10 * 1.50 + (30)*0.40+ (dist-40)*0.40;

            }else{
                logger.info("the dist is > 60.0 and all good");
                remb = 10 * 1.50 + 30 * 0.40 + 20*0.55 + ((int)(dist - 60)/(int)20) * 6.81;
            }
            return String.valueOf(remb);

        }catch (Exception e){
            logger.error("The user enters a string");
            String remb = "0.0";
            return remb;
        }

    }

}
