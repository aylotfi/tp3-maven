#base image
FROM openjdk:11-jre-slim-buster
COPY ./target/tp3-0.0.1-SNAPSHOT.jar  ./
CMD ["java", "-jar", "tp3-0.0.1-SNAPSHOT.jar"]